package vessla.demo.webappsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsrfProtectionTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsrfProtectionTestApplication.class, args);
	}
}
