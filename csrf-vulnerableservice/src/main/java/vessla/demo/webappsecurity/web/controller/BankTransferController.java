package vessla.demo.webappsecurity.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BankTransferController {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/*
	 * A static variable is used here just to avoid adding an
	 * embedded data storage mechanism to the simple demo project
	 */
	private static int accountBalance = 1000;
	
	/*
	 * Method for checking the account balance after the attack 
	 * (performed by the logged in user)
	 */
	@GetMapping("/balance")
    public String getBalance(Model model) {
        logger.info("Just checking the account balance...");
        model.addAttribute("balance", accountBalance);
        
        return "complete";
    }
	
	/*
	 * Sensitive API mapped to the GET request:
	 * Operation can be performed even if CSRF protection is enabled!
	 */
	@GetMapping("/transfer")
    public String transfer(Model model, @RequestParam("accountNo") int accountNo, 
      @RequestParam("amount") final int amount) {
        logger.info("Transfer via GET to {}", accountNo);
        accountBalance -= amount;
        model.addAttribute("balance", accountBalance);
        
        return "complete";
    }
	
	/*
	 * Sensitive API mapped to the POST request:
	 * Operation will be blocked if a valid CSRF token is not added to the request
	 */
	@PostMapping("/transfer")
    public String transferViaPost(Model model, @RequestParam("accountNo") int accountNo, 
      @RequestParam("amount") final int amount) {
        logger.info("Transfer via POST to {}", accountNo);
        accountBalance -= amount;
        model.addAttribute("balance", accountBalance);
        
        return "complete";
    }
	
}
