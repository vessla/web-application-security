package vessla.demo.webappsecurity.model;

public class Message {

	private String contents;
	
	public Message(){
		this("default contents");
	}
	
	public Message(String contents){
		this.contents = contents;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}
	
	
}
