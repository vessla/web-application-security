package vessla.demo.webappsecurity.web.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import vessla.demo.webappsecurity.model.Message;

@Controller
@RequestMapping("/posts")
public class XssProtectionTestController {
	
	private static ArrayList<Message> posts = new ArrayList<>();
	
	static{
		posts.add(new Message("<i>Some example post in italics</>"));
	}

	@GetMapping("/all") 
	public String listProducts(Model model){
		model.addAttribute("posts", posts); 
		return "posts";
	}
	
	@GetMapping("/new") 
	public String displayNewProductForm(Model model){
		model.addAttribute("newMessage", new Message());
		return "new";
	}
	
	@PostMapping("/new")
    public String saveProduct(@ModelAttribute("newMessage") Message newMessage) {
		posts.add(newMessage);
		return "redirect:/posts/all";
    }

}
