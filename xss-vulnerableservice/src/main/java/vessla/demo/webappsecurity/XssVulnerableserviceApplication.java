package vessla.demo.webappsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XssVulnerableserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(XssVulnerableserviceApplication.class, args);
	}
}
