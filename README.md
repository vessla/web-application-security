# README #

### What is this repository for? ###

This repository contains a set of demo projects illustrating security-related aspects of designing web applications. 
All of the projects are only meant to illustrate a single concept and thus kept as simple as possible (avoiding even embedded database dependencies and using in-memory authentication with hard coded credentials). 
These examples are not intended to be treated as production-ready solutions.

Each concept consists of a brief theoretical introduction, a vulnerable service source code and an exploit implementation. 

## List of the described concepts

1. CSRF

	See [this document](https://bitbucket.org/vessla/webappsecurity/raw/0f029df1bd0d87fdaa8b5012ce7a9e2d30de385a/csrf-description.docx) for a brief theoretical description. 
	
	* The code of the vulnerable application is available [here](https://bitbucket.org/vessla/webappsecurity/src/20f9cc1097b9160928df923bd7cb6fe57184ec4c/csrf-vulnerableservice/?at=master)
	* The code of the malicious web application - [here](https://bitbucket.org/vessla/webappsecurity/src/20f9cc1097b9160928df923bd7cb6fe57184ec4c/csrf-exploit/?at=master)


2. XSS

	See [this document](https://bitbucket.org/vessla/webappsecurity/raw/2ff6323456985817f907927f7203ddbf30748c25/xss-description.docx) for a brief theoretical description. 
	
	* The code of the vulnerable application is available [here](https://bitbucket.org/vessla/webappsecurity/src/436e3e4c9cefd7f4b5c904e9a8e2f0af8ab5edc4/xss-vulnerableservice/?at=master)


### Who do I talk to? ###

pjadamska@gmail.com